# download OIDN
#if [ ! -e ./opt ] ; then
#	export https_proxy=192.168.0.17:8001
#	curl -L -O  'https://github.com/OpenImageDenoise/oidn/releases/download/v1.4.2/oidn-1.4.2.x86_64.linux.tar.gz'
#	mkdir -p ./opt
#	tar -C ./opt -xvzf /tmp/oidn-1.4.2.x86_64.linux.tar.gz
#fi

# download and extract Nuke
#wget -q -P /tmp/ https://thefoundry.s3.amazonaws.com/products/nuke/releases/12.2v5/Nuke-12.2v5-linux-x86-64-installer.tgz
#tar -C /tmp -xvzf /tmp/Nuke-12.2v5-linux-x86-64-installer.tgz
#cd /tmp
#./Nuke-12.2v5-linux-x86-64-installer.run --accept-foundry-eula --prefix=/usr/local
#rm -vf /tmp/*

# build plugin
#cd $CI_PROJECT_DIR

export LD_LIBRARY_PATH=/atomo/pipeline/libs/linux/x86_64/gcc-multi/mpfr/3.1.4/lib/:$LD_LIBRARY_PATH
export CC=/atomo/pipeline/libs/linux/x86_64/gcc-multi/gcc/4.8.3/bin/gcc
export CXX=/atomo/pipeline/libs/linux/x86_64/gcc-multi/gcc/4.8.3/bin/g++


for NV in $(ls -1 /atomo/apps/linux/x86_64/nuke/ | sort -V | tail -3) ; do
	rm -rf build
	mkdir -p build && cd build

	sudo mkdir -p /atomo/pipeline/tools/nuke/$NV/script/
	sudo chmod a+rwx /atomo/pipeline/tools/nuke/$NV/script/ 
	cmake \
		-DCMAKE_INSTALL_PREFIX=/atomo/pipeline/tools/nuke/$NV/script/ \
		-DOPENIMAGEDENOISE_ROOT_DIR=/atomo/pipeline/libs/linux/x86_64/gcc-multi/oidn/1.4.2/ \
		-DNUKE_ROOT=/atomo/apps/linux/x86_64/nuke/$NV \
	..
	make -j 8 VERBOSE=1 && make install
	cd ..
	rm ~/.nuke/init.py
done
